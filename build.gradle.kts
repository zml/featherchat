import ca.stellardrift.build.common.adventure
import ca.stellardrift.build.common.agpl3

plugins {
    val opinionatedVersion = "4.1"
    id("ca.stellardrift.opinionated") version opinionatedVersion
    id("net.kyori.indra.license-header") version "1.2.1"
    id("net.kyori.indra.publishing") version "1.2.1"
    id("ca.stellardrift.configurate-transformations") version opinionatedVersion apply false
    id("ca.stellardrift.templating") version opinionatedVersion apply false
    id("ca.stellardrift.localization") version opinionatedVersion apply false
    id("com.github.johnrengelman.shadow") version "6.0.0" apply false
    id("com.github.ben-manes.versions") version "0.36.0"
}

group = "ca.stellardrift"
version = "2.0-SNAPSHOT"
description = "A plugin that allows simple modification of chat and other messages"

subprojects {
    apply(plugin = "ca.stellardrift.opinionated")
    apply(plugin = "net.kyori.indra.license-header")
    apply(plugin = "net.kyori.indra.publishing")
}

allprojects {
    repositories {
        maven("https://repo.stellardrift.ca/repository/stable/") {
            name = "stellardriftReleases"
            mavenContent { releasesOnly() }
        }

        maven("https://repo.stellardrift.ca/repository/snapshots/") {
            name = "stellardriftSnapshots"
            mavenContent { snapshotsOnly() }
        }
    }

    indra {
        gitlab("stellardrift", "featherchat")
        agpl3()

        configurePublications {
            pom {
                developers {
                    developer {
                        id.set("zml")
                        email.set("zml at \$groupId")
                    }
                }
            }
        }

        publishReleasesTo("stellardrift", "https://repo.stellardrift.ca/repository/releases/")
        publishSnapshotsTo("stellardrift", "https://repo.stellardrift.ca/repository/snapshots/")
    }

    dependencies {
        testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
        testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.0")
    }

    license {
        header = rootProject.file("HEADER")
    }

    tasks.processResources.configure { dependsOn(tasks.updateLicenses) }
    tasks.checkLicenses.configure { mustRunAfter(tasks.updateLicenses) }
    tasks.check.configure { dependsOn(tasks.checkLicenses) }
}

dependencies {
    api(platform(adventure("bom", "4.3.0")))
    api("net.kyori:adventure-api")
    api("com.github.spullara.mustache.java:compiler:0.9.6") {
        exclude(group = "com.google.guava", module = "guava")
    }
    implementation("org.spongepowered:configurate-hocon:4.0.0")

    implementation("jakarta.xml.bind:jakarta.xml.bind-api:2.3.3")
    implementation("com.sun.xml.bind:jaxb-impl:2.3.3")
}
