/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import net.kyori.adventure.text.BuildableComponent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentBuilder;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;

import java.net.URL;
import java.util.stream.Collector;

public class TextFormats {
    public static final Component EQUALS_TEXT = Component.text('=');
    public static final Component COMMA_SEP = Component.text(", ");
    public static final Component SPACE_TEXT = Component.space();

    private TextFormats() {
    }

    public static Component normal(ComponentBuilder<?, ?> builder) {
        return builder.color(NamedTextColor.DARK_AQUA).build();
    }

    public static Component normal(Component text) {
        return text.color(NamedTextColor.DARK_AQUA);
    }

    public static <C extends BuildableComponent<C, ?>> C hl(ComponentBuilder<C, ?> builder) {
        return builder.color(NamedTextColor.AQUA).build();
    }

    public static Component hl(Component text) {
        return text.color(NamedTextColor.AQUA);
    }

    public static <C extends BuildableComponent<C, ?>> C link(ComponentBuilder<C, ?> builder, URL url) {
        return hl(builder.decoration(TextDecoration.UNDERLINED, true)
                .clickEvent(ClickEvent.openUrl(url))
                .hoverEvent(HoverEvent.showText(Component.text(url.toString()))));
    }

    public static <C extends BuildableComponent<C, B>, B extends ComponentBuilder<C, B>> C link(C text, URL url) {
        return link(text.toBuilder(), url);
    }

    public static <B extends ComponentBuilder<?, B>> B suggestCommand(B builder, String command) {
        return builder.decoration(TextDecoration.UNDERLINED, true)
                .clickEvent(ClickEvent.suggestCommand(command))
                .hoverEvent(HoverEvent.showText(Component.text(command)));
    }

    public static <B extends ComponentBuilder<?, B>> B runCommand(B builder, String command) {
        return builder.decoration(TextDecoration.UNDERLINED, true)
                .clickEvent(ClickEvent.runCommand(command))
                .hoverEvent(HoverEvent.showText(Component.text(command)));
    }

    public static Collector<Component, ? extends ComponentBuilder<?, ?>, Component> toJoinedText(Component separator) {
        return Collector.of(Component::text,
                (builder, add) -> {
                    /*if (!builder.isEmpty()) {
                        builder.append(separator);
                    }**/
                    builder.append(add);
                }, (a, b) -> {
                    /*if (!a.isEmpty()) {
                        a.append(separator);
                    }*/
                    a.append(b.asComponent());
                    return a;
                }, TextComponent.Builder::build);
    }
}
