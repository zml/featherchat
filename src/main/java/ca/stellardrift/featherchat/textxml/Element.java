/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentBuilder;
import net.kyori.adventure.text.ScoreComponent;
import net.kyori.adventure.text.SelectorComponent;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.TranslatableComponent;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.TextDecoration;
import org.checkerframework.checker.nullness.qual.Nullable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@XmlSeeAlso({
        A.class,
        B.class,
        Br.class,
        Color.class,
        I.class,
        Obfuscated.class,
        Strikethrough.class,
        Span.class,
        Tr.class,
        U.class,
        Selector.class,
        Score.class
    })
public abstract class Element {

    @XmlAttribute
    @Nullable
    private String onClick = null;

    @XmlAttribute
    @Nullable
    private String onShiftClick = null;

    @XmlAttribute
    @Nullable
    private String onHover = null;

    @XmlElementRef(type = Element.class)
    @XmlMixed
    protected List<Object> mixedContent = new ArrayList<>();

    protected abstract void modifyBuilder(ComponentBuilder<?, ?> builder);

    private static final Pattern FUNCTION_PATTERN = Pattern.compile("^([^(]+)\\('(.*)'\\)$");

    protected void applyTextActions(ComponentBuilder<?, ?> builder) throws Exception {
        if (this.onClick != null) {
            final Matcher matcher = FUNCTION_PATTERN.matcher(this.onClick);
            if (!matcher.matches()) {
                throw new MessageParseException("Invalid onClick handler in " + getClass().getSimpleName() + " tag.");
            }

            final String eventType = matcher.group(1);
            final String eventString = matcher.group(2);


            builder.clickEvent(TextEventUtil.deserializeClickAction(eventType, eventString));
        }

        if (this.onShiftClick != null) {
            final Matcher matcher = FUNCTION_PATTERN.matcher(this.onShiftClick);
            if (!matcher.matches()) {
                throw new MessageParseException("Invalid onShiftClick handler in " + getClass().getSimpleName() + " tag.");
            }

            final String eventType = matcher.group(1);
            final String eventString = matcher.group(2);

            if (!eventType.equalsIgnoreCase("insert_text")) {
                throw new MessageParseException("Unknown insertion action " + eventType + " in " + getClass().getSimpleName() + " tag.");
            }
            builder.insertion(eventString);
        }

        if (this.onHover != null) {
            final Matcher matcher = FUNCTION_PATTERN.matcher(this.onHover);
            if (!matcher.matches()) {
                throw new MessageParseException("Invalid onHover handler in " + getClass().getSimpleName() + " tag.");
            }

            final String eventType = matcher.group(1);
            final String eventString = matcher.group(2);
            builder.hoverEvent(TextEventUtil.deserializeHoverAction(eventType, eventString));
        }

    }

    public ComponentBuilder<?, ?> toComponent() throws Exception {
        ComponentBuilder<?, ?> builder;
        if (this.mixedContent.size() == 0) {
            builder = Component.text();
        } else if (this.mixedContent.size() == 1) { // then we are a thin wrapper around the child
            builder = builderFromObject(this.mixedContent.get(0));
        } else {
            if (this.mixedContent.get(0) instanceof String) {
                builder = builderFromObject(this.mixedContent.get(0));
                this.mixedContent.remove(0);
            } else {
                builder = Component.text();
            }
            for (Object child : this.mixedContent) {
                builder.append(builderFromObject(child).build());
            }
        }

        modifyBuilder(builder);
        applyTextActions(builder);

        return builder;
    }

    protected ComponentBuilder<?, ?> builderFromObject(Object o) throws Exception {
        if (o instanceof String) {
            return Component.text().content(String.valueOf(o).replace('\u000B', ' '));
        } else if (o instanceof Element) {
            return ((Element) o).toComponent();
        } else {
            throw new IllegalArgumentException("What is this even? " + o);
        }
    }

    public static Element fromComponent(Component text) throws IOException {
        final AtomicReference<Element> fixedRoot = new AtomicReference<>();
        Element currentElement = null;
        if (text.color() != null) {
            currentElement = update(fixedRoot, currentElement, new Color.C(text.color()));
        }

        if (text.style().hasDecoration(TextDecoration.BOLD)) {
            currentElement = update(fixedRoot, currentElement, new B());
        }

        if (text.style().hasDecoration(TextDecoration.ITALIC)) {
            currentElement = update(fixedRoot, currentElement, new I());
        }

        if (text.style().hasDecoration(TextDecoration.OBFUSCATED)) {
            currentElement = update(fixedRoot, currentElement, new Obfuscated.O());
        }

        if (text.style().hasDecoration(TextDecoration.STRIKETHROUGH)) {
            currentElement = update(fixedRoot, currentElement, new Strikethrough.S());
        }

        if (text.style().hasDecoration(TextDecoration.UNDERLINED)) {
            currentElement = update(fixedRoot, currentElement, new U());
        }

        final @Nullable ClickEvent click = text.clickEvent();
        if (click != null) {
            if (click.action() == ClickEvent.Action.OPEN_URL) {
                currentElement = update(fixedRoot, currentElement, new A(new URL(click.value())));
            } else {
                if (currentElement == null) {
                    fixedRoot.set(currentElement = new Span());
                }
                currentElement.onClick = TextEventUtil.getEventName(click) + "('" + click.value() + "')";
            }
        } else {
            if (currentElement == null) {
                fixedRoot.set(currentElement = new Span());
            }
        }

        final @Nullable HoverEvent<?> hoverEvent = text.hoverEvent();
        if (hoverEvent != null) {
            currentElement.onHover = TextEventUtil.getEventName(hoverEvent) + "('" + TextEventUtil.serializeEventResult(hoverEvent) + "')";
        }

        final @Nullable String insertion = text.insertion();
        if (insertion != null) {
            currentElement.onShiftClick = "insert_text('" + insertion+ "')";
        }

        if (text instanceof TextComponent) {
            currentElement.mixedContent.add(((TextComponent) text).content());
        } else if (text instanceof TranslatableComponent) {
            currentElement = update(fixedRoot, currentElement, new Tr(((TranslatableComponent) text).key()));
            for (Component o : ((TranslatableComponent) text).args()) {
                currentElement.mixedContent.add(Element.fromComponent(o));
            }
        } else if (text instanceof ScoreComponent) {
            final ScoreComponent score = (ScoreComponent) text;
            Score el = new Score(score.objective(), score.value());
            el.mixedContent.add(score.name());
            currentElement.mixedContent.add(el);
        } else if (text instanceof SelectorComponent) {
            currentElement.mixedContent.add(new Selector(((SelectorComponent) text).pattern()));
        } else {
            throw new IllegalArgumentException("Text was of type " + text.getClass() + ", which is unsupported by the XML format");
        }

        for (Component child : text.children()) {
            currentElement.mixedContent.add(Element.fromComponent(child));
        }

        return fixedRoot.get();
    }

    private static Element update(AtomicReference<Element> fixedRoot, @Nullable Element parent, Element child) {
        if (parent == null) {
            fixedRoot.set(child);
        } else {
            parent.mixedContent.add(child);
        }
        return child;
    }
}
