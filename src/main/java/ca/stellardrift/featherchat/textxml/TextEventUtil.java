/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import org.checkerframework.checker.nullness.qual.Nullable;

class TextEventUtil {
    private TextEventUtil() {
        throw new RuntimeException("I'm a utility class!");
    }

    static String getEventName(ClickEvent click) {
        if (!click.action().readable()) {
            throw new UnsupportedOperationException("Click events with action " + click.action() + " cannot be serialized");
        }
        return ClickEvent.Action.NAMES.key(click.action());
    }

    static String getEventName(HoverEvent<?> hover) {
        if (!hover.action().readable()) {
            throw new UnsupportedOperationException("Hover events with action " + hover.action() + " cannot be serialized");
        }
        return HoverEvent.Action.NAMES.key(hover.action());
    }

    static String serializeEventResult(HoverEvent<?> action) {
        if (action.value() instanceof Component) {
            return XmlComponentSerializer.get().serialize((Component) action.value());
        } else {
            return "{}";
            //throw new UnsupportedOperationException("TODO"); // TODO: figure out this whole thing
        }
    }

    static ClickEvent deserializeClickAction(String key, String args) {
        final ClickEvent.@Nullable Action action = ClickEvent.Action.NAMES.value(key);
        if (action == null) {
            throw new IllegalArgumentException("Invalid click event type " + key);
        }
        return ClickEvent.clickEvent(action, args);
    }

    static HoverEvent<?> deserializeHoverAction(String key, String args) {
        final HoverEvent.Action<?> action = HoverEvent.Action.NAMES.value(key);
        if (action == null) {
            throw new IllegalArgumentException("Invalid hover action type " + key);
        } else if (action == HoverEvent.Action.SHOW_TEXT) {
            return HoverEvent.showText(XmlComponentSerializer.get().deserialize(args));
        } else {
            return null;
            //throw new IllegalArgumentException("Unknown");
        }
    }
}
