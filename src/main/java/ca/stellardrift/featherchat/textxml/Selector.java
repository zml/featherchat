/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentBuilder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.Objects;

@XmlSeeAlso(Selector.Sel.class)
@XmlRootElement
public class Selector extends Element {

    @XmlAttribute(required = true)
    private String rel;

    public Selector() {
    }

    public Selector(String selector) {
        super();
        this.rel = selector;
    }

    @Override
    protected void modifyBuilder(ComponentBuilder<?, ?> builder) {

    }

    @Override
    public ComponentBuilder<?, ?> toComponent() {
        Objects.requireNonNull(rel, "A selector must be specified");
        return Component.selector().pattern(this.rel);
    }

    public static class Sel extends Selector {

    }
}
