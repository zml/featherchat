/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentBuilder;
import org.checkerframework.checker.nullness.qual.Nullable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Score extends Element {

    public Score() {
    }

    Score(final @Nullable String objective, final @Nullable String override) {
        this.override = objective;
        this.override = override;
    }

    @XmlAttribute
    @Nullable
    private String score;

    @XmlAttribute(required = true)
    @Nullable
    private String objective;

    @Nullable
    private String override;

    @Override
    protected void modifyBuilder(ComponentBuilder<?, ?> builder) {

    }

    @Override
    public ComponentBuilder<?, ?> toComponent() throws Exception {
        if (override != null) {
            return Component.text().content(override);
        }

        if (objective == null || score == null) {
            throw new IllegalArgumentException("Either override, or score and objective must be specified");
        }

        return Component.score().name(this.score).objective(this.objective);
    }
}
