/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.ComponentBuilder;
import net.kyori.adventure.text.format.TextDecoration;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso(Strikethrough.S.class)
@XmlRootElement
public class Strikethrough extends Element {

    @Override
    protected void modifyBuilder(ComponentBuilder<?, ?> builder) {
        builder.decoration(TextDecoration.STRIKETHROUGH, true);
    }

    @XmlRootElement
    public static class S extends Strikethrough {

    }
}
