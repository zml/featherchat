/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.vars;

import com.google.common.collect.ImmutableMap;
import net.kyori.adventure.text.Component;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.network.status.StatusClient;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.util.Nameable;

public class Variables {
    private static final String PLACEHOLDER_MESSAGE = "message";
    private Variables() {
    }

    public static Object channelVariables(String channelName) {
        return new ChannelVariables(channelName);
    }

    public static Object subject(Subject source) {
        return new CommandSourceNameVariables(source);
    }

    public static Object nameable(Nameable named) {
        return new NameableVariables(named);
    }

    public static Object player(ServerPlayer player) {
        return new PlayerScoreboardVariables(player);
    }

    public static Object statusClient(StatusClient client) {
        return new StatusClientVariables(client);
    }

    public static Object message(String message) {
        return ImmutableMap.of(PLACEHOLDER_MESSAGE, message);
    }

    public static Object message(Component message) {
        return ImmutableMap.of(PLACEHOLDER_MESSAGE, message);
    }
}
