/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.channel;

import ca.stellardrift.featherchat.Configuration;
import ca.stellardrift.featherchat.FeatherChatMustacheFactory;
import ca.stellardrift.featherchat.Permissions;
import ca.stellardrift.featherchat.TemplateProvider;
import com.github.mustachejava.Mustache;
import com.google.inject.Inject;
import org.checkerframework.checker.nullness.qual.MonotonicNonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.service.permission.SubjectData;
import org.spongepowered.api.util.Nameable;
import org.spongepowered.api.util.Tristate;
import org.spongepowered.api.world.Locatable;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Comment;
import org.spongepowered.configurate.serialize.SerializationException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Configuration object containing the configuration information useful for a channel
 */
public class Channel implements TemplateProvider {
    private final String name;
    private final String fallbackPerm;
    private final Configuration parent;
    private @MonotonicNonNull ChannelConfiguration config;

    @ConfigSerializable
    static class ChannelConfiguration {
        transient final FeatherChatMustacheFactory factory;
        transient final Configuration parent;

        @Comment("Format overrides for this channel")
        Map<String, String> formats = new HashMap<>();
        @Comment("Range for receiving messages. -1 is infinite range")
        int range = -1;
        @Comment("The permission to broadcast to")
        @Nullable String permission;

        @Inject
        ChannelConfiguration(Configuration parent) {
            this.parent = parent;
            this.factory = new FeatherChatMustacheFactory(s -> {
                final String fmt;
                if (formats.containsKey(s)) {
                    fmt = formats.get(s);
                } else {
                    fmt = parent.getMessagesNode().node(s).getString("");
                }
                return fmt;
            });
        }

    }

    public Channel(String name, final Configuration config) throws SerializationException {
        this.name = name;
        this.parent = config;
        this.fallbackPerm = Permissions.PERM_CHANNEL + "." + this.name;
        reload(config);
    }

    public String name() {
        return name;
    }

    // -- Load/save

    public void reload(final Configuration config) throws SerializationException {
        this.config = config.getChannelsNode().node(this.name).get(ChannelConfiguration.class, (Supplier<ChannelConfiguration>) () -> new ChannelConfiguration(config));
    }

    public void save(final Configuration config) throws SerializationException {
        config.getChannelsNode().node(this.name).set(ChannelConfiguration.class, this.config);
    }

    // -- Configuration data access

    public Integer range() {
        return this.config.range;
    }

    public String permission() {
        return this.config.permission == null ? this.fallbackPerm : this.config.permission;
    }

    @Override
    public boolean hasTemplate(String identifier) {
        return this.config.factory.hasTemplate(identifier);
    }

    @Override
    public @Nullable Mustache template(String template) {
        return hasTemplate(template) ? this.config.factory.compile(template) : null;
    }

    @Override
    public Mustache fragment(String template, String fragmentKey) {
        return this.config.factory.fragment(template, fragmentKey);
    }

    @Override
    public void refreshTemplate(String template) {
        this.config.factory.refreshTemplate(template);
    }

    // -- Manage membership

    public void addSource(Subject source) {
        if (!source.hasPermission(permission())) {
            source.getTransientSubjectData().setPermission(SubjectData.GLOBAL_CONTEXT, permission(), Tristate.TRUE);
        }
    }

    public void removeSource(Subject source) {
        if (source.hasPermission(permission())) {
            source.getTransientSubjectData().setPermission(SubjectData.GLOBAL_CONTEXT, permission(), Tristate.UNDEFINED);
        }
    }

    public boolean sendsTo(Subject source) {
        return source.hasPermission(permission());
    }

    /**
     * Get a set of CommandSources that have this channel as their active talking channel
     * @return sources that default to this channel
     */
    public Set<ServerPlayer> talkers() {
        return parent.getGame().getServer().getOnlinePlayers().stream().filter(input ->
                input.getChatRouter() instanceof AbstractChannelInstance &&
                        ((AbstractChannelInstance) input.getChatRouter()).getConfig().equals(Channel.this))
                .collect(Collectors.toSet());
    }

    public Set<Subject> receivers() {
        // TODO: Restore Subject -> Something mapping
        //PermissionService service = parent.getGame().getServer().getServiceProvider().permissionService();
       // return service.getLoadedCollections().values().stream()
                //.flatMap(collect -> collect.getLoadedWithPermission(permission()).keySet().stream())
        return this.parent.getGame().getServer().getOnlinePlayers().stream()
                .filter(subj -> subj.hasPermission(permission()))
                /*.map(Subject::getCommandSource)
                .filter(Optional::isPresent)
                .map(Optional::get)*/
                .collect(Collectors.toSet());
    }

    public AbstractChannelInstance createInstanceFor(Nameable source) {
        AbstractChannelInstance channel = new PermissionChannelInstance(this, source.getName(), parent.getGame());
        if (range() > 0 && source instanceof Locatable) {
            channel = new RangeChannelInstance(channel, (Locatable) source);
        }
        return channel;
    }
}
