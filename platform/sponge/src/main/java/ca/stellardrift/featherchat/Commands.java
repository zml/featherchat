/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import ca.stellardrift.featherchat.motd.MotdController;
import ca.stellardrift.featherchat.vars.Variables;
import com.google.common.xml.XmlEscapers;
import net.kyori.adventure.audience.MessageType;
import net.kyori.adventure.identity.Identified;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.adventure.SpongeComponents;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.command.CommandCause;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.command.exception.CommandPermissionException;
import org.spongepowered.api.command.parameter.CommandContext;
import org.spongepowered.api.command.parameter.Parameter;
import org.spongepowered.api.command.parameter.managed.Flag;
import org.spongepowered.api.data.Keys;
import org.spongepowered.api.entity.living.player.PlayerChatRouter;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.EventContextKeys;
import org.spongepowered.api.service.permission.SubjectData;
import org.spongepowered.api.util.Tristate;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.function.Consumer;

import static ca.stellardrift.featherchat.TextFormats.hl;
import static net.kyori.adventure.identity.Identity.nil;

public class Commands {

    private static final URL AGPL_URL;
    private static final URL SOURCE_URL;

    static {
        try {
            AGPL_URL = new URL(ProjectData.LICENSE_URL);
            //noinspection ConstantConditions
            if (ProjectData.VERSION.endsWith("SNAPSHOT")) {
                SOURCE_URL = new URL(ProjectData.SOURCE_URL);
            } else {
                SOURCE_URL = new URL(ProjectData.SOURCE_URL + "/tree/" + ProjectData.VERSION);
            }
        } catch (MalformedURLException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private Commands() {}

    static Command.Parameterized featherChat(final FeatherChatPlugin plugin, final MotdController motd) {
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_ROOT_DESCRIPTION.t())
                .setExecutor(ctx -> {
                    ctx.sendMessage(nil(), TextFormats.normal(Component.text().content("FeatherChat v").append(hl(Component.text(ProjectData.VERSION)))));
                    ctx.sendMessage(nil(), TextFormats.normal(Messages.COMMANDS_ROOT_LICENSING.tB(
                            ProjectData.NAME,
                            TextFormats.link(Messages.COMMANDS_ROOT_AGPL.tB(), AGPL_URL),
                            TextFormats.link(Component.text(SOURCE_URL.toString()), SOURCE_URL)
                    )));
                    ctx.sendMessage(nil(), TextFormats.normal(Messages.COMMANDS_ROOT_GIT.t(ProjectData.GIT_DESCRIBE)));
                    return CommandResult.success();
                })
                .child(Command.builder()
                        .setPermission(Permissions.PERM_RELOAD)
                        .setShortDescription(Messages.COMMANDS_RELOAD_DESCRIPTION.t())
                        .setExecutor(ctx -> {
                            plugin.reload(ctx.getCause().getAudience());
                            return CommandResult.success();
                        }).build(), "reload", "rel"
                )
                .child(plugin.configuration().createEditCommand("/" + plugin.container().getMetadata().getId() + ":chat edit"), "edit", "e")
                .child(motd.getMotdCommand(), "motd", "joinmsg")
                .build();
    }

    static Command.Parameterized mute(final FeatherChatPlugin plugin) {
        final Parameter.Value<ServerPlayer> players = Parameter.player().consumeAllRemaining().setKey("players").build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_MUTE_DESCRIPTION.t())
                .parameter(players)
                .setPermission(Permissions.PERM_MUTE)
                .setExecutor(ctx -> {
                    int successfulTargets = 0;
                    for (ServerPlayer target : ctx.getAll(players)) {
                        if (target.hasPermission(SubjectData.GLOBAL_CONTEXT, Permissions.PERM_CHAT)) {
                            target.getTransientSubjectData().setPermission(SubjectData.GLOBAL_CONTEXT, Permissions.PERM_CHAT, Tristate.FALSE);
                            ctx.sendMessage(nil(), Messages.COMMANDS_MUTE_SUCCESS.t(target.getName()));
                        }
                    }
                    return CommandResult.builder().setResult(successfulTargets).build();
                })
                .build();
    }

    static Command.Parameterized unmute(final FeatherChatPlugin plugin) {
        final Parameter.Value<ServerPlayer> players = Parameter.player().consumeAllRemaining().setKey("players").build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_UNMUTE_DESCRIPTION.t())
                .parameter(players)
                .setPermission(Permissions.PERM_UNMUTE)
                .setExecutor(ctx -> {
                    return new MultiTargetAction<ServerPlayer>(Permissions.PERM_UNMUTE, ctx.getCause().getCause(), ctx.getAll(players)) {
                        @Override
                        protected Component getTargetNotificationMessage(ServerPlayer target) {
                            return Messages.COMMANDS_UNMUTE_SUCCESS_TARGET.t(target);
                        }

                        @Override
                        protected Component getSourceNotificationMessage(Collection<ServerPlayer> targets) {
                            return Messages.COMMANDS_UNMUTE_SUCCESS_SOURCE.t(this.source, targets.stream()
                                    .map(ply -> ply.get(Keys.DISPLAY_NAME).get())
                                    .collect(TextFormats.toJoinedText(TextFormats.COMMA_SEP)));
                        }

                        @Override
                        protected void actSingle(ServerPlayer target) {
                            if (target.hasPermission(SubjectData.GLOBAL_CONTEXT, Permissions.PERM_CHAT)) {
                                target.getTransientSubjectData().setPermission(SubjectData.GLOBAL_CONTEXT, Permissions.PERM_CHAT, Tristate.FALSE);
                            }
                        }
                    }.actToResult();
                })
                .build();

    }

    static Command.Parameterized me(final FeatherChatPlugin plugin) {
        final Parameter.Value<String> message = Parameter.remainingJoinedStrings().setKey("message").build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_ME_DESCRIPTION.t())
                .parameter(message)
                .setPermission(Permissions.PERM_ME)
                .setExecutor(ctx -> {
                    TemplateProvider provider = plugin.templateProvider(ctx);
                    String rawMessage = ctx.requireOne(message);
                    if (!ctx.hasPermission(Permissions.PERM_CHAT_FORMATTED_XML)) {
                        rawMessage = XmlEscapers.xmlContentEscaper().escape(rawMessage);
                    }

                    final Component parsed;
                    try {
                        String finalRawMessage = rawMessage;
                        parsed = provider.processOrThrow(TemplateProvider.TEMPLATE_EMOTE, vars -> {
                            vars.add(Variables.message(finalRawMessage));
                            plugin.populateVariables(ctx.getCause().getCause(), vars);
                        }, () -> new CommandException(Messages.COMMANDS_ME_ERROR_DISABLED.t()));
                    } catch (RuntimeException e) {
                        throw new CommandException(Messages.ACTION_CHAT_PARSE_ERROR.t(e.getMessage()), e);
                    }

                    //messageTarget(ctx, plugin).chat();
                    plugin.game().getServer().sendMessage(identity(ctx), parsed, MessageType.CHAT); // TODO: permission message types
                    //ctx.getCause().getAudience().sendMessage(parsed, MessageType.CHAT);
                    return CommandResult.success();
                }).build();
    }

    static Command.Parameterized say(final FeatherChatPlugin plugin) {
        final Parameter.Value<String> messageParam = Parameter.remainingJoinedStrings().setKey("message").build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_SAY_DESCRIPTION.t())
                .parameter(messageParam)
                .setPermission(Permissions.PERM_CHAT)
                .setExecutor(ctx -> {
                    final String message = ctx.requireOne(messageParam);
                    @Nullable Component msg = plugin.handleChat(plugin.templateProvider(ctx), ctx.getCause().getCause(), message);
                    if (msg != null) {
                        plugin.game().getServer().sendMessage(identity(ctx), msg, MessageType.CHAT);
                        return CommandResult.success();
                    } else {
                        throw new CommandException(Messages.COMMANDS_SAY_ERROR_NO_MESSAGE.t());
                    }
                })
                .build();
    }

    static Command.Parameterized stop(final FeatherChatPlugin plugin) {
        final Flag acceptStop = Flag.builder().alias("y").build();
        final Parameter.Value<String> stopMessage = Parameter.remainingJoinedStrings().setKey("message").optional().build();

        return Command.builder()
                .setShortDescription(Messages.COMMANDS_STOP_DESCRIPTION.t())
                .setPermission(Permissions.PERM_STOP)
                .flag(acceptStop)
                .parameter(stopMessage)
                .setExecutor(ctx -> {
                    int numOnlinePlayers = plugin.game().getServer().getOnlinePlayers().size();
                    @Nullable String commandArg = ctx.getOne(stopMessage).orElse(null);
                    Consumer<CommandCause> stopAction = callbackSrc -> {
                        for (ServerPlayer pl : plugin.game().getServer().getOnlinePlayers()) {
                            @Nullable Component value = plugin.getPossibleStoredMessage(plugin.templateProvider(pl), TemplateProvider.TEMPLATE_STOP_KICK, commandArg, ctx.getCause().getCause().with(pl));
                            if (value != null) {
                                pl.kick(value);
                            }
                        }

                        @Nullable Component kickTmpl = plugin.getPossibleStoredMessage(plugin.configuration().getDefaultChannel(), TemplateProvider.TEMPLATE_STOP_KICK, commandArg, null);
                        if (kickTmpl == null) {
                            kickTmpl = Component.translatable("multiplayer.disconnect.server_shutdown");
                        }
                        plugin.logger().info(Messages.COMMANDS_STOP_NOTICE_CONSOLE.t(hl(Component.text().content(callbackSrc.getFriendlyIdentifier().orElse(callbackSrc.getIdentifier()))), hl(kickTmpl)));
                        plugin.game().getServer().shutdown();
                    };

                    if (ctx.hasFlag(acceptStop)) {
                        stopAction.accept(ctx.getCause());
                    } else {
                        Component confirmationLink = (ctx.getCause().getContext().containsKey(EventContextKeys.PLAYER) ? Messages.COMMANDS_STOP_CONFIRM : Messages.COMMANDS_STOP_CONFIRM_CONSOLE).tB()
                                .color(NamedTextColor.AQUA).decoration(TextDecoration.UNDERLINED, true)
                                .clickEvent(SpongeComponents.executeCallback(stopAction)).build();


                        ctx.sendMessage(nil(), Messages.COMMANDS_STOP_WARNING.tB().color(NamedTextColor.BLUE).append(TextFormats.SPACE_TEXT, confirmationLink).build());
                    }
                    return CommandResult.builder().setResult(numOnlinePlayers).build();
                }).build();
    }

    // Helpers //


    public static PlayerChatRouter messageTarget(final CommandContext source, final FeatherChatPlugin plugin) {
        // TODO: MessageSender interface/way to get chat router
        final @Nullable ServerPlayer player = source.getCause().first(ServerPlayer.class).orElse(null);
        if (player != null && player.getChatRouter() instanceof TemplateProvider) {
            return player.getChatRouter();
        } else {
            return PlayerChatRouter.toAudience(plugin.game().getServer());
        }
    }

    public static void checkPermission(final CommandContext src, final String permission) throws CommandPermissionException {
        if (!src.hasPermission(permission)) {
            throw new CommandPermissionException(Messages.COMMANDS_GENERAL_ERROR_PERMISSION.t());
        }
    }

    public static void checkPermission(final CommandContext src, final String permission1, final String permission2) throws CommandPermissionException {
        checkPermission(src, permission1 + '.' + permission2);
    }

    public static Identity identity(final CommandContext ctx) {
        return ctx.getCause().getContext().get(EventContextKeys.PLAYER).map(Identified::identity).orElse(Identity.nil());
    }
}
