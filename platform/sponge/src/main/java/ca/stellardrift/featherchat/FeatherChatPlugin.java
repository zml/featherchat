/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import ca.stellardrift.featherchat.channel.ChannelCommands;
import ca.stellardrift.featherchat.channel.ChannelListener;
import ca.stellardrift.featherchat.filter.FilterHandler;
import ca.stellardrift.featherchat.motd.MotdController;
import ca.stellardrift.featherchat.textxml.XmlComponentSerializer;
import ca.stellardrift.featherchat.vars.Variables;
import com.google.common.collect.Lists;
import com.google.common.xml.XmlEscapers;
import com.google.inject.Inject;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.serializer.ComponentSerializer;
import net.kyori.adventure.text.serializer.plain.PlainComponentSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Game;
import org.spongepowered.api.Server;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.command.parameter.CommandContext;
import org.spongepowered.api.data.Keys;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.Cause;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.lifecycle.ConstructPluginEvent;
import org.spongepowered.api.event.lifecycle.RefreshGameEvent;
import org.spongepowered.api.event.lifecycle.RegisterCommandEvent;
import org.spongepowered.api.event.lifecycle.StartingEngineEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.event.message.PlayerChatEvent;
import org.spongepowered.api.event.network.ServerSideConnectionEvent;
import org.spongepowered.api.event.server.ClientPingServerEvent;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.util.Nameable;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.plugin.PluginContainer;
import org.spongepowered.plugin.jvm.Plugin;

import java.util.List;
import java.util.function.Consumer;

/**
 * A simple sponge plugin
 */
@Plugin(ProjectData.ARTIFACT_ID)
public class FeatherChatPlugin {

    private static ComponentSerializer<Component, Component, String> xmlSerial() {
        return XmlComponentSerializer.get();
    }

    private final Logger logger;
    private final Configuration config;
    private final Game game;
    private final PluginContainer container;
    private final TabListListener tabList;
    private final MotdController motd;

    @Inject
    public FeatherChatPlugin(Configuration config, Game game, PluginContainer container, TabListListener tabList, MotdController motd) {
        this.logger = LogManager.getLogger(ProjectData.NAME, new TranslatableMessageFactory()); // TODO: Upstream this
        this.config = config;
        this.game = game;
        this.container = container;
        this.tabList = tabList;
        this.motd = motd;
    }

    @Listener
    public void onReloadRequest(RefreshGameEvent event) {
        Audience src = event.getCause().first(Audience.class).orElse(game.getSystemSubject());
        try {
            reload(src);
        } catch (CommandException e) {
            logger.error(e.getMessage(), e.getCause());
        }
    }

    void reload(Audience notifier) throws CommandException {
        try {
            reload();
        } catch (final ConfigurateException e) {
            throw new CommandException(Messages.COMMANDS_RELOAD_ERROR.t(e.getMessage()), e);
        }
        notifier.sendMessage(Messages.COMMANDS_RELOAD_SUCCESS.t());
    }

    @Listener
    public void onPreInit(ConstructPluginEvent event) {
        try {
            reload();
        } catch (final ConfigurateException e) {
            logger.warn(Messages.INTERNAL_ERROR_CONFIG_INIT.t(), e);
        }
        xmlSerial().deserialize("<span/>"); // Warm up the text XML serializer

        game.getEventManager().registerListeners(this.container, new ChannelListener(configuration()));
        game.getEventManager().registerListeners(this.container, new FilterHandler(configuration()));
        game.getEventManager().registerListeners(this.container, this.motd);
        game.getEventManager().registerListeners(this.container, this.tabList);
    }

    @Listener
    public void registerCommands(RegisterCommandEvent<Command.Parameterized> event) {

        event.register(this.container, Commands.featherChat(this, this.motd), "featherchat", "chat");
        event.register(this.container, Commands.mute(this), "mute", "quiet");
        event.register(this.container, Commands.unmute(this), "unmute", "unquiet");

        if (this.config.shouldRegisterOverridingCommands()) {
            event.register(this.container, Commands.me(this), "me", "emote", "action");
            event.register(this.container, Commands.say(this), "say", "echo", "quote");
            event.register(this.container, Commands.stop(this), "stop", "end");
        }

        event.register(this.container, ChannelCommands.createChannelCommands(this), "channel", "chan");
    }

    @Nullable Component getPossibleStoredMessage(TemplateProvider prov, String templateKey, @Nullable String templateString, @Nullable Cause variableSource) {
        Consumer<List<Object>> vars = variableSource == null ? list -> {} : list -> populateVariables(variableSource, list);
        if (templateString != null) {
            return prov.processFragmentToText(templateString, templateKey, vars);
        }
        return prov.processToText(templateKey, vars);
    }


    private void reload() throws ConfigurateException {
        this.config.load();
    }

    @Listener
    public void onServerListQuery(ClientPingServerEvent event) {
        TemplateProvider provider = configuration().getDefaultChannel(); // players hasn't joined, so we don't get a channel to send to.
        try {
            final @Nullable Component response = provider.processToText(TemplateProvider.TEMPLATE_PING_MOTD, vars ->
                    vars.add(Variables.statusClient(event.getClient())));
            if (response != null) {
                event.getResponse().setDescription(response);
            }
        } catch (RuntimeException e) {
            logger.error(Messages.INTERNAL_ERROR_PING_FMT.t(event.getClient().getAddress(), e.getLocalizedMessage()));
        }
    }

    @Listener
    public void onLogin(ServerSideConnectionEvent.Join event) {
        TemplateProvider provider = templateProvider(event);
        final List<Object> vars = populateVariables(event.getCause(), Lists.newArrayList());

        try {
            final @Nullable Component joinMessage = provider.processToText(TemplateProvider.TEMPLATE_JOIN, list -> list.addAll(vars));
            if (joinMessage != null) {
                event.setMessage(joinMessage);
            }
        } catch (RuntimeException e) {
            logger.error(Messages.INTERNAL_ERROR_JOIN_FMT.t(event.getPlayer().getName(), e.getLocalizedMessage()));
        }

        try {
            // TODO: How to respond to changes
            final @Nullable Component displayName = provider.processToText(TemplateProvider.TEMPLATE_PLAYER_NAME, list -> list.addAll(vars));
            if (displayName != null) {
                event.getPlayer().offer(Keys.DISPLAY_NAME, displayName);
                event.getPlayer().offer(Keys.IS_CUSTOM_NAME_VISIBLE, true);
            }
        } catch (RuntimeException e) {
            logger.error(Messages.INTERNAL_ERROR_DISPNAME_FMT.t(event.getPlayer().getName(), e.getLocalizedMessage()));
        }
    }

    @Listener
    public void onQuit(ServerSideConnectionEvent.Disconnect event) {
        try {
            final @Nullable Component value = templateProvider(event).processToText(TemplateProvider.TEMPLATE_QUIT, vars -> populateVariables(event.getCause(), vars));
            if (value != null) {
                event.setMessage(value);
            }
        } catch (RuntimeException e) {
            logger.error(Messages.INTERNAL_ERROR_QUIT_FMT.t(event.getPlayer().getName(), e.getLocalizedMessage()));
        }
    }

    @Listener
    public void onChat(PlayerChatEvent event) {
        event.getCause().first(ServerPlayer.class).ifPresent(player -> {
            if (!player.hasPermission(Permissions.PERM_CHAT)) {
                player.sendMessage(Messages.ACTION_CHAT_DENY.tB().color(NamedTextColor.DARK_RED).build());
                event.setCancelled(true);
                return;
            }

            // TODO: template provider from event chat router
            @Nullable Component newMessage = handleChat(templateProvider(player), event.getCause(), PlainComponentSerializer.plain().serialize(event.getOriginalMessage()));
            if (newMessage != null) {
                event.setMessage(newMessage);
            }
        });
    }

    @Listener
    public void registerPermissions(final StartingEngineEvent<Server> event) {
        Permissions.registerDefaultPermissions(event.getEngine().getServiceProvider().permissionService());
    }

    @Nullable Component handleChat(TemplateProvider target, Cause src, String message) {
        final boolean canChatFormatted = src.first(Subject.class).map(it -> it.hasPermission(Permissions.PERM_CHAT_FORMATTED_XML)).orElse(true);
        if (!canChatFormatted) {
            message = XmlEscapers.xmlContentEscaper().escape(message);
        }

        try {
            String finalMessage = message;
            return target.processToText(TemplateProvider.TEMPLATE_CHAT, vars -> {
                vars.add(Variables.message(finalMessage));
                populateVariables(src, vars);
            });
        } catch (RuntimeException e) {
            src.first(Audience.class).ifPresent(aud -> aud.sendMessage(Messages.ACTION_CHAT_PARSE_ERROR.tB(e.getMessage()).color(NamedTextColor.RED).build()));
            logger.error("Unable to parse chat message for user {}, error: {}", src.first(Nameable.class).orElse(() -> "unknown").getName(), e.getLocalizedMessage());
            logger.error("Stacktrace: ", e);
            return null;
        }
    }

    List<Object> populateVariables(Cause source, List<Object> chatFormatVariables) {
        source.first(Subject.class).ifPresent(subj -> chatFormatVariables.add(Variables.subject(subj)));
        source.first(Nameable.class).ifPresent(nameable -> chatFormatVariables.add(Variables.nameable(nameable)));

        return chatFormatVariables;
    }

    public TemplateProvider templateProvider(final CommandContext context) {
        return configuration().getDefaultChannel(); // TODO: MessageSender things
    }

    public TemplateProvider templateProvider(ServerPlayer source) {
        if (source.getChatRouter() instanceof TemplateProvider) {
            return (TemplateProvider) source.getChatRouter();
        }
        return configuration().getDefaultChannel();
    }

    private TemplateProvider templateProvider(MessageChannelEvent event) {
        final @Nullable TemplateProvider channel = (TemplateProvider) event.getAudience()
                .filter(it -> it instanceof TemplateProvider)
                .orElse(null);

        if (channel != null) {
            return channel;
        }
        return configuration().getDefaultChannel();
    }

    public Game game() {
        return this.game;
    }

    public Configuration configuration() {
        return this.config;
    }

    public PluginContainer container() {
        return this.container;
    }

    public MotdController motd() {
        return this.motd;
    }

    public Logger logger() {
        return this.logger;
    }
}
