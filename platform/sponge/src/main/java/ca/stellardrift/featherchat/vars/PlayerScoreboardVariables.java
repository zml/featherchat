/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.vars;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.scoreboard.Team;

class PlayerScoreboardVariables {
    private final ServerPlayer player;

    public PlayerScoreboardVariables(ServerPlayer player) {
        this.player = player;
    }

    private @Nullable Team getTeam() {
        return player.getScoreboard().getMemberTeam(player.getTeamRepresentation()).orElse(null);
    }

    public Object team() {
        final @Nullable Team team = getTeam();
        return team == null ? "" : team.getDisplayName();
    }

    public Object teamPrefix() {
        final @Nullable Team team = getTeam();
        return team == null ? "" : team.getPrefix();
    }

    public Object teamSuffix() {
        final @Nullable Team team = getTeam();
        return team == null ? "" : team.getSuffix();
    }
}
