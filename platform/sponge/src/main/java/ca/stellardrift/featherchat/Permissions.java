/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.service.permission.SubjectData;
import org.spongepowered.api.util.Tristate;

public class Permissions {
    public static final String PERM_CHANNEL = "featherchat.channel.receive";
    public static final String PERM_CHAT_RECEIVE = "featherchat.chat.receive";
    public static final String PERM_CHAT = "featherchat.chat.send";
    public static final String PERM_CHAT_FORMATTED_XML = "featherchat.chat.formatted";
    public static final String PERM_ME = "featherchat.command.me";
    public static final String PERM_RELOAD = "featherchat.command.reload";
    public static final String PERM_EDIT = "featherchat.command.edit";
    public static final String PERM_MUTE = "featherchat.command.mute";
    public static final String PERM_UNMUTE = "featherchat.command.unmute";
    public static final String PERM_JOIN_CHANNEL = "featherchat.channel.join";
    public static final String PERM_LEAVE_CHANNEL = "featherchat.channel.leave";
    public static final String PERM_EDIT_CHANNEL = "featherchat.channel.edit";
    public static final String PERM_DELETE_CHANNEL = "featherchat.channel.delete";
    public static final String PERM_TALKIN_CHANNEL = "featherchat.channel.talk";
    public static final String PERM_STOP = "minecraft.command.stop";
    public static final String PERM_MOTD_LIST = "featherchat.command.motd.list";
    public static final String PERM_MOTD_INFO = "featherchat.command.motd.info";
    public static final String PERM_MOTD_EDIT = "featherchat.command.motd.edit";
    public static final String PERM_MOTD_DELETE = "featherchat.command.motd.delete";

    private Permissions() {
    }

    public static String forSpecific(String permission, String child) {
        return permission + "." + child;
    }

    static void registerDefaultPermissions(PermissionService service) {
        final SubjectData defaultData = service.getDefaults().getTransientSubjectData();
        defaultData.setPermission(SubjectData.GLOBAL_CONTEXT, PERM_CHANNEL, Tristate.FALSE);
        defaultData.setPermission(SubjectData.GLOBAL_CONTEXT, PERM_CHAT_FORMATTED_XML, Tristate.FALSE);
        defaultData.setPermission(SubjectData.GLOBAL_CONTEXT, PERM_CHAT_RECEIVE, Tristate.TRUE);
        defaultData.setPermission(SubjectData.GLOBAL_CONTEXT, PERM_CHAT, Tristate.TRUE);
        defaultData.setPermission(SubjectData.GLOBAL_CONTEXT, PERM_ME, Tristate.TRUE);
    }
}
