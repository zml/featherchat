/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.vars;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.registry.RegistryTypes;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.world.Locatable;
import org.spongepowered.api.world.World;

class CommandSourceNameVariables {
    private final Subject source;

    public CommandSourceNameVariables(Subject source) {
        this.source = source;
    }

    public String prefix() {
        return source.getOption("prefix").orElse("");
    }

    public String suffix() {
        return source.getOption("suffix").orElse("");
    }

    public String world() {
        return source instanceof Locatable ? ((Locatable) source).getServerLocation().getWorldKey().getFormatted() : "";
    }

    public String dim() {
        if (source instanceof Locatable) {
            final World<?, ?> world = ((Locatable) source).getWorld();
            return world.registries().registry(RegistryTypes.DIMENSION_TYPE).valueKey(world.getDimensionType()).getFormatted();
        } else {
            return "";
        }
    }

    public @Nullable String option(String key) {
        return source.getOption(key).orElse(null);
    }
}
