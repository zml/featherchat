/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.filter;

import ca.stellardrift.featherchat.Configuration;
import ca.stellardrift.featherchat.Messages;
import ca.stellardrift.featherchat.TemplateProvider;
import ca.stellardrift.featherchat.vars.Variables;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.plain.PlainComponentSerializer;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.message.PlayerChatEvent;

/**
 * Handler of message filters
 */
public class FilterHandler {
    private final Configuration config;

    public FilterHandler(Configuration config) {
        this.config = config;
    }

    @Listener
    public void onPlayerChat(PlayerChatEvent event) {
        event.getCause().first(ServerPlayer.class).ifPresent(player -> {
            final TemplateProvider tmpl = config.templateProvider(player);
            String plainMessage = PlainComponentSerializer.plain().serialize(event.getOriginalMessage());
            for (FilterEntry entry : config.getFilterEntries()) {
                if (entry.getPattern() == null) {
                    continue;
                }

                if (entry.getPattern().matcher(plainMessage).find()) {
                    if (!entry.isExempt(player)) {
                        event.setCancelled(true);
                        final @Nullable String denyMessage = entry.getDenyMessage();
                        if (denyMessage == null) {
                            player.sendMessage(Messages.FILTER_DENIED.t());
                            break;
                        }

                        try {
                            player.sendMessage(tmpl.processFragmentToText(entry.getDenyMessage(), "filter-" + entry.getPattern() + "-deny", vars -> {
                                vars.add(Variables.subject(player));
                                vars.add(Variables.player(player));
                            }));
                        } catch (RuntimeException ex) {
                            player.sendMessage(Component.text(denyMessage));
                        }
                        break;
                    }
                }
            }
        });
    }

}
