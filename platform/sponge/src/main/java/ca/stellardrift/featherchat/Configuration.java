/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import ca.stellardrift.featherchat.channel.Channel;
import ca.stellardrift.featherchat.filter.FilterEntry;
import ca.stellardrift.featherchat.motd.MotdEntry;
import com.github.mustachejava.MustacheException;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.leangen.geantyref.TypeToken;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.MonotonicNonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Game;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.command.parameter.Parameter;
import org.spongepowered.api.command.parameter.managed.standard.VariableValueParameters;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.configurate.loader.ConfigurationLoader;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializerCollection;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static net.kyori.adventure.identity.Identity.nil;

@Singleton
public class Configuration {
    private static final TypeToken<Map<String, MotdEntry>> MOTDS_ENTRY_TYPE = new TypeToken<Map<String, MotdEntry>>() {};
    private static final URL MUSTACHE_URL;
    private static final URL TOKEN_WIKI_URL;

    static {
        try {
            MUSTACHE_URL = new URL("http://mustache.github.io");
            TOKEN_WIKI_URL = new URL("https://gitlab.com/zml/featherchat/-/wikis/Tokens");
        } catch (MalformedURLException e) {
            throw new ExceptionInInitializerError(e); // Should be treated as constants
        }
    }

    private final ConfigurationLoader<CommentedConfigurationNode> configLoader;
    @MonotonicNonNull
    private ConfigurationNode config;
    private final Logger logger;
    private final Game game;
    private final TypeSerializerCollection serializers;
    private final LoadingCache<String, Channel> channelConfigs = CacheBuilder.newBuilder().build(
            new CacheLoader<String, Channel>() {
                @Override
                public Channel load(String key) throws Exception {
                    return new Channel(key, Configuration.this);
                }
            });
    @MonotonicNonNull
    private List<FilterEntry> filterEntries;
    @MonotonicNonNull
    private Map<String, MotdEntry> motdEntries;


    @Inject
    public Configuration(final @DefaultConfig(sharedRoot = true) ConfigurationLoader<CommentedConfigurationNode> config,
                         final Logger logger, final Game game, final TypeSerializerCollection serializers) {
        this.configLoader = config;

        this.logger = logger;
        this.game = game;
        this.serializers = serializers;
    }

    public void load() throws ConfigurateException {
        @Nullable ConfigurateException possibleEx = null;
        final ConfigurationLoader<CommentedConfigurationNode> defaultsLoader = HoconConfigurationLoader.builder()
                .defaultOptions(options ->
                        options.serializers(this.serializers)
                        .shouldCopyDefaults(true)
                        .implicitInitialization(true))
                .url(getClass().getResource("default.conf"))
                .build();
        try {
            this.config = this.configLoader.load();
            this.config.mergeFrom(defaultsLoader.load());
            this.configLoader.save(this.config);
        } catch (final ConfigurateException ex) {
            if (this.config == null) {
                this.config = defaultsLoader.load();
            }
            possibleEx = ex;

        }

        for (Channel config : this.channelConfigs.asMap().values()) {
                config.reload(this);
        }
        this.filterEntries = getFiltersNode().getList(FilterEntry.class);
        this.motdEntries = getMotdsNode().get(MOTDS_ENTRY_TYPE, (Supplier<Map<String, MotdEntry>>) HashMap::new);

        if (possibleEx != null) {
            throw possibleEx;
        }
    }

    public void save() throws ConfigurateException {
        getMotdsNode().set(MOTDS_ENTRY_TYPE, this.motdEntries);
        this.configLoader.save(this.config);
    }

    public ConfigurationNode getMessagesNode() {
        return this.config.node("messages");
    }

    public ConfigurationNode getChannelsNode() {
        return this.config.node("channels");
    }

    public Iterable<String> getChannelNames() {
        return getChannelsNode().childrenMap().keySet().stream()
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    public Channel getChannel(String channel) {
        try {
            return channelConfigs.get(channel.toLowerCase());
        } catch (ExecutionException e) { // TODO: Handle better
            throw new RuntimeException(e);
        }
    }

    public void removeChannel(String channel) {
        channel = Preconditions.checkNotNull(channel, "channel").toLowerCase();
        getChannelsNode().node(channel).raw(null);
        this.channelConfigs.invalidate(channel);
    }

    /**
     * Set the template at key to a new template.
     * @param key The template identifier
     * @param value The new template
     *
     * <!--@throws MustacheException If the provided template is not a valid mustache template.-->
     */
    public void setTemplate(String key, String value) throws SerializationException {
        //this.factory.compile(new StringReader(value), "<test>");
        getMessagesNode().node(key).set(value);
        for (Channel config : this.channelConfigs.asMap().values()) {
            config.refreshTemplate(key);
        }
    }

    public Command.Parameterized createEditCommand(final String parentName) {
        final Parameter.Value<String> templateParam = Parameter.builder(TypeToken.get(String.class), () ->
                VariableValueParameters.dynamicChoicesBuilder(String.class)
                .setChoices(() -> getMessagesNode().childrenMap().keySet()
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.toSet()))
                        .setResults(Function.identity())
                .build())
                .setKey("template").build();
        final Parameter.Value<String> valueParam = Parameter.remainingJoinedStrings().setKey("value").build();

        return Command.builder()
                .setShortDescription(Messages.COMMANDS_EDIT_DESCRIPTION.t())
                .setPermission(Permissions.PERM_EDIT)
                .parameter(Parameter.seqBuilder(templateParam).then(valueParam).terminal().build()) // TODO: is ugly
                .setExecutor(ctx -> {
                    if (ctx.hasAny(templateParam)) { // A template is specified, we are setting its value
                        final String template = ctx.requireOne(templateParam);
                        final String value = ctx.requireOne(valueParam);
                        try {
                            setTemplate(template, value);
                        } catch (MustacheException | SerializationException ex) {
                            throw new CommandException(Messages.COMMANDS_EDIT_SET_ERROR_INVALID_FORMAT.t(ctx, template, ex.getLocalizedMessage()));
                        }
                        try {
                            save();
                        } catch (final ConfigurateException ex) {
                            this.logger.warn(Messages.COMMANDS_EDIT_SET_ERROR_SAVE_CONSOLE.t(ctx.getCause()), ex);
                            throw new CommandException(Messages.COMMANDS_EDIT_SET_ERROR_SAVE.t(ctx, template));
                        }
                        ctx.sendMessage(nil(), Messages.COMMANDS_EDIT_SET_SUCCESS.t(template));
                        return CommandResult.success();
                    } else {
                        List<Component> contents = new ArrayList<>();
                        for (Map.Entry<Object, ? extends ConfigurationNode> ent : getMessagesNode().childrenMap().entrySet()) {
                            final String key = ent.getKey().toString();
                            final String value = ent.getValue().getString("<none>");
                            TextComponent.Builder labelBuilder = Component.text().content(key);
                            labelBuilder.decoration(TextDecoration.UNDERLINED, true)
                                    .clickEvent(ClickEvent.suggestCommand(parentName + " " + key + " " + value));

                            contents.add(Component.join(TextFormats.EQUALS_TEXT, TextFormats.hl(labelBuilder), Component.text(value, NamedTextColor.WHITE)));
                        }
                        PaginationList.Builder builder = game.getServiceProvider().paginationService().builder();
                        builder.title(TextFormats.hl(Messages.COMMANDS_EDIT_LIST_HEADER.tB().decoration(TextDecoration.BOLD, true)))
                            .header(Messages.COMMANDS_EDIT_LIST_DESCRIPTION.tB(ctx,
                                TextFormats.link(Messages.COMMANDS_EDIT_LIST_DESCRIPTION_MUSTACHE.tB(ctx), MUSTACHE_URL),
                                TextFormats.link(Messages.COMMANDS_EDIT_LIST_DESCRIPTION_WIKI_TOKEN.tB(ctx), TOKEN_WIKI_URL)).color(NamedTextColor.GRAY).build())
                            .contents(contents)
                            .sendTo(ctx.getCause().getAudience());
                        return CommandResult.empty();
                    }
                })
                .build();
    }

    public Game getGame() {
        return this.game;
    }

    public Channel getDefaultChannel() {
        return getChannel(config.node("default-channel").getString("global"));
    }

    public Iterable<FilterEntry> getFilterEntries() {
        return filterEntries;
    }

    private ConfigurationNode getFiltersNode() {
        return this.config.node("filters");
    }

    public Map<String, MotdEntry> getMotdEntries() {
        return motdEntries;
    }

    private ConfigurationNode getMotdsNode() {
        return this.config.node("motds");
    }

    public boolean shouldRegisterOverridingCommands() {
        return this.config.node("register-overriding-commands").getBoolean(true);
    }

    public TemplateProvider templateProvider(ServerPlayer source) {
        if (source.getChatRouter() instanceof TemplateProvider) {
            return (TemplateProvider) source.getChatRouter();
        }
        return getDefaultChannel();
    }

    public TemplateProvider templateProvider(MessageChannelEvent event) {
        final Optional<Audience> eventAudience = event.getAudience();
        if (eventAudience.isPresent() && eventAudience.get() instanceof TemplateProvider) {
            return (TemplateProvider) eventAudience.get();
        }
        return getDefaultChannel();
    }

    public Logger getLogger() {
        return this.logger;
    }

    public TypeSerializerCollection serializers() {
        return this.serializers;
    }
}
