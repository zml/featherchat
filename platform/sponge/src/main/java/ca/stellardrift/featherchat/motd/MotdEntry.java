/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.motd;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;


@ConfigSerializable
public class MotdEntry {

    // Populated from configuration
    private String message;
    private @Nullable String permission;
    private boolean enabled = true;

    public MotdEntry() {
    }

    MotdEntry(String message, @Nullable String permission, boolean enabled) {
        this.message = message;
        this.permission = permission;
        this.enabled = enabled;
    }

    public boolean shouldSendTo(Subject subject) {
        return enabled && (permission == null || subject.hasPermission(permission));
    }

    public String getMessage() {
        return this.message;
    }

    public MotdEntry withMessage(String message) {
        return new MotdEntry(message, this.permission, this.enabled);
    }

    public MotdEntry withPermission(@Nullable String permission) {
        return new MotdEntry(this.message, permission, this.enabled);
    }

    public MotdEntry withEnabled(boolean enabled) {
        return new MotdEntry(this.message, this.permission, enabled);
    }


    public @Nullable String getPermission() {
        return this.permission;
    }

    public boolean isEnabled() {
        return this.enabled;
    }
}
