/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.channel;

import ca.stellardrift.featherchat.Configuration;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.network.ServerSideConnectionEvent;

public class ChannelListener {
    private final Configuration config;

    public ChannelListener(Configuration config) {
        this.config = config;
    }

    @Listener(order = Order.EARLY)
    public void onJoin(ServerSideConnectionEvent.Join event) {
        AbstractChannelInstance channel = config.getDefaultChannel().createInstanceFor(event.getPlayer());
        event.getPlayer().setChatRouter(channel);
        event.setAudience(channel);
    }
}
