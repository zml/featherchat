/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.filter;

import ca.stellardrift.featherchat.Messages;
import ca.stellardrift.featherchat.textxml.XmlComponentSerializer;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.event.Cause;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

import java.util.regex.Pattern;

/**
 * An entry of text to block. This provides the necessary data structure as well as relevant utility methods.
 */
@ConfigSerializable
public class FilterEntry {

    private boolean enabled = true;
    private @Nullable Pattern pattern;
    private @Nullable String exemptPermission;
    private @Nullable String denyMessage;

    /**
     * Get the pattern to match against
     *
     * @return The compiled pattern to match against
     */
    public @Nullable Pattern getPattern() {
        return pattern;
    }

    /**
     * Check if a source is exempt from being affected by this filter, through the exemption permission, the filter being disabled,
     * or other future means
     *
     * @param source The source to check for exemptness
     * @return whether this source is exempt
     */
    public boolean isExempt(Subject source) {
        return !enabled || (exemptPermission != null && source.hasPermission(exemptPermission));
    }

    public @Nullable String getDenyMessage() {
        return this.denyMessage;
    }

    /**
     * Get the message to send to users who have been denied from chatting by this filter.
     *
     * @param src The sender of the message that was denied
     * @return The message to send upon denying chat
     */
    public Component getDenyMessage(Cause src) {
        if (denyMessage == null) {
            return Messages.FILTER_DENIED.t(src);
        } else {
            try {
                return XmlComponentSerializer.get().deserialize(this.denyMessage);
            } catch (IllegalArgumentException e) {
                return Component.text(this.denyMessage);
            }
        }
    }
}
