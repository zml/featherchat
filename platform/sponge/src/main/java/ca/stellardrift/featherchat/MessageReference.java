/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.*;
import net.kyori.adventure.text.format.NamedTextColor;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.util.locale.LocaleSource;

import java.util.*;
import java.util.function.Function;

import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.Component.translatable;

public final class MessageReference {
    private final String bundleName;
    private final String key;
    private final String combinedKey;

    MessageReference(String bundleName, String key) {
        this.bundleName = bundleName;
        this.key = key;
        this.combinedKey = bundleName + '.' + key;
    }

    public String getKey() {
        return this.key;
    }

    public String getValue(Locale locale) {
        return ResourceBundle.getBundle(bundleName, locale).getString(key);
    }

    public Component t(ComponentLike... args) {
        return TextFormats.normal(translatable().key(this.combinedKey).args(args));
    }

    public Component t(Object... args) {
        return TextFormats.normal(translatable().key(this.combinedKey).args(componentize(args)));
    }

    public TranslatableComponent.Builder tB(Object... args) {
        return translatable().key(this.combinedKey).args(componentize(args));
    }

    public TranslatableComponent.Builder tB(ComponentLike... args) {
        return translatable().key(this.combinedKey).args(args).color(NamedTextColor.DARK_AQUA);
    }

    public Component toComponent() {
        return toComponent(TextFormats::normal);
    }

    public Component toComponent(Function<ComponentBuilder<?, ?>, Component> styler) {
        return styler.apply(translatable().key(this.combinedKey));
    }

    public void throwCommand(Object... args) throws CommandException {
        throw new CommandException(t(args));
    }

    private static List<Component> componentize(final Object... args) {
        final List<Component> result = new ArrayList<>(args.length);
        for (final Object arg : args) {
            if (arg instanceof ComponentLike) {
                result.add(((ComponentLike) arg).asComponent());
            } else {
                result.add(text(String.valueOf(arg)));
            }
        }
        return result;
    }

    private static Locale getLocale(Audience src) {
        return src instanceof LocaleSource ? ((LocaleSource) src).getLocale() : Locale.getDefault();
    }
}
