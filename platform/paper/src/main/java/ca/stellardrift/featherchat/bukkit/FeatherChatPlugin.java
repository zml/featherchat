/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.bukkit;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.bossbar.BossBar;
import net.kyori.adventure.platform.bukkit.BukkitAudiences;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.serializer.craftbukkit.BukkitComponentSerializer;
import net.kyori.adventure.title.Title;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.checkerframework.checker.nullness.qual.MonotonicNonNull;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.function.Consumer;

import static net.kyori.adventure.text.format.TextColor.color;

public class FeatherChatPlugin extends JavaPlugin {
    private static final TextColor RESPONSE_COLOR = color(0x33AC88);
    private static final TextColor BAR_COLOR = color(0xCC0044);
    private static final Duration DEF = Duration.of(5, ChronoUnit.SECONDS);
    private static final Title.Times TITLE_TIMES = Title.Times.of(DEF, DEF, DEF);
    private @MonotonicNonNull BukkitAudiences adventure;

    @Override
    public void onEnable() {
        this.adventure = BukkitAudiences.create(this);
    }

    @Override
    public void onDisable() {
        if (this.adventure != null) {
            this.adventure.close();
        }
    }

    private static String join(String[] elements, String separator, int startIdx) {
        final StringBuilder ret = new StringBuilder();
        for (int i = startIdx; i < elements.length; ++i) {
            if (i != startIdx) {
                ret.append(separator);
            }
            ret.append(elements[i]);
        }
        return ret.toString();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        final Audience result = this.adventure.sender(sender);
        if (args.length < 1) {
            return false;
        }
        switch (args[0]) {
            case "countdown":
                beginCountdown(Component.text("Until the end", BAR_COLOR), 10, result, viewer -> {
                    viewer.sendMessage(Component.text("Countdown complete", RESPONSE_COLOR));
                    viewer.sendActionBar(Component.text("Countdown complete", RESPONSE_COLOR));
                });
                break;
            case "bar":
                if (!(sender instanceof Player)) {
                    return true;
                }
                result.sendActionBar(Component.text("Test"));
                break;

            case "title":
                if (args.length < 2) {
                    result.sendMessage(Component.text("Usage: title <title>"));
                    return false;
                }
                final String titleStr = join(args, " ", 1);
                final Component title = BukkitComponentSerializer.gson().deserialize(titleStr);
                result.showTitle(Title.title(title, Component.text("From adventure"), Title.DEFAULT_TIMES));
                break;
            case "version":
                result.sendMessage(Component.text(b -> b.content("Adventure platform").color(NamedTextColor.DARK_PURPLE)));
                break;
            case "echo":
                final String value = join(args, " ", 1);
                final Component text = BukkitComponentSerializer.gson().deserialize(value);
                result.sendMessage(text);
                break;
            default:
                result.sendMessage(Component.text("Unknown sub-command: " + args[0], NamedTextColor.RED));
                return false;
        }
        return true;
    }

    /**
     * Boss bar animation update frequency, in ticks
     */
    private static final int UPDATE_FREQUENCY = 2;


    /**
     * Begin a countdown shown on a boss bar, completing with the specified action
     *
     * @param title Boss bar title
     * @param timeSeconds seconds boss bar will last
     * @param targets viewers of the action
     * @param completionAction callback to execute when countdown is complete
     */
    private void beginCountdown(Component title, final int timeSeconds, Audience targets, Consumer<Audience> completionAction) {
        final BossBar bar = BossBar.bossBar(title, 1, BossBar.Color.RED, BossBar.Overlay.PROGRESS);

        final int timeMs = timeSeconds * 1000; // total time ms
        final long[] times = new long[] {timeMs, System.currentTimeMillis()}; // remaining time in ms, last update time
        getServer().getScheduler().runTaskTimer(this, task -> {
            final long now = System.currentTimeMillis();
            final long dt = now - times[1];
            times[0] -= dt;
            times[1] = now;

            if (times[0] <= 0) { // we are complete
                task.cancel();
                targets.hideBossBar(bar);
                completionAction.accept(targets);
                return;
            }

            final float newFraction = bar.progress() - (dt / (float) timeMs);
            assert newFraction > 0;
            bar.progress(newFraction);
        }, 0, UPDATE_FREQUENCY);
        targets.showBossBar(bar);
    }
}
